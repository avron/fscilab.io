---
title: "Code of Conduct"
date: 2020-09-25T01:44:58+05:30
author: false
draft: false
---
# FSCI - Code of Conduct & Inclusion Policy

Free Software Community of India is committed to the philosophy of Free Software. FSCI welcomes and encourages participation by everyone.

No matter how you identify yourself or how others perceive you: we welcome you. We welcome contributions from everyone as long as they interact constructively with our community.

While much of the work for our project is technical in nature, we value and encourage contributions from those with expertise in other areas, and welcome them into our community.

FSCI has adopted a code of conduct for participants to its discussion forums, chat groups, channels and other modes of communication within the community. We respect diversity and promote inclusion. We do not tolerate harassment of participants in any form. We recognize The Universal Declaration of Human Rights and expect everyone to uphold the rights mentioned in it.

## Applicability and Scope

This code of conduct applies to all of this community's spaces, including public channels, private channels and direct messages, both online and off.

## Toward a Welcoming and Safe Community

At FSCI we recognize that there are many dimensions of diversity and this may include, but is not limited to:

*  background
*  family status
*  gender
*  gender identity or expression
*  race
*  caste
*  religion or lack thereof
*  food preferences
*  ethnicity
*  national origin
*  nationality
*  immigration status
*  age
*  sex
*  sexual orientation
*  native language
*  language
*  education
*  ability
*  neuro(a)typicality
*  physical appearance
*  pregnancy status
*  health status
*  veteran status
*  political affiliation
*  marital status
*  indigenousness
*  employment status
*  financial status
*  any other dimension of diversity

We recognize that an individual lives at the intersection of various dimensions of diversity and this makes every individual unique.

At FSCI a diverse, inclusive, and equitable community is one where all volunteers feels valued and respected. We are committed to a nondiscriminatory approach and provide equal opportunity for participation and collaboration in all of our activities. We respect and value diverse life experiences and heritages and ensure that all voices are valued and heard.

We hope to create an environment in which diverse individuals can collaborate and interact in a positive and affirming way. Examples of behavior that contributes to creating this sort of environment include:

* Using welcoming and inclusive language
* Being respectful of differing viewpoints and experiences
* Gracefully accepting constructive criticism
* Focusing on what is best for the overall community
* Showing empathy towards other community members

## Inclusive Language

Use inclusive language whenever possible.

* Unless the gender of a singular personal antecedent is otherwise specified, use the gender-neutral singular pronouns "they", "them", "their", and "theirs".
* Avoid using "he" as a universal pronoun; likewise, avoid using binary alternatives such as "he/she", "he or she", or "(s)he".
* Use “humankind”, “humanity” or “human race” instead of “mankind”.
* Use “artificial” or “human-caused” instead of "man-made".
* Use "everybody" or "people" instead of "guys".

## Anti-Harassment Statement

This community will not tolerate harassment of any kind. Examples of harassment include:

* Offensive comments related to any dimension(s) of diversity.
* Unwelcome comments regarding a person's lifestyle choices and practices, including thos related to food, health, parenting, relationships, drugs, and employment.
* Deliberate misgendering, using inappropriate pronouns, or use of "dead" or rejected nams.
* Gratuitous or off-topic sexual images or behavior in spaces where they're not appropriae.
* Physical contact and simulated physical contact (eg. textual descriptions like "hug" or"backrub") without consent or after a request to stop.
* Threats of violence.
* Incitement of violence towards any individual or group, including encouraging a person o commit suicide or to engage in self-harm.
* Deliberate intimidation.
* Stalking or following - online or in the physical world.
* Harassing photography or recording, including logging online activity for harassment puposes.
* Sustained disruption of discussion.
* Unwelcome sexual attention.
* Patterns of inappropriate social contact, such as requesting/assuming inappropriate levls of intimacy with others.
* Continued one-on-one communication after requests to cease.
* Deliberate "outing" of any aspect of a person's identity without their consent except as necessary to protect vulnerable people from intentional abuse.
* Publication of non-harassing private communication.
* Jokes that resemble the above, such as "hipster racism", still count as harassment even if meant satirically or ironically.

## Actions to Increase Representation

From time to time various events or programs maybe organized and executed with the aim of increasing representation from underrepresented groups. This code of conduct does not prohibit positive discrimination that may be required to achieve the objectives of such events or programs. This extends to the exclusion of related philosophical viewpoints (like "open source") to highlight and emphasis the Free Software philosophy.
Expected Behavior

1. **Be respectful**

   In a community, inevitably there will be people with whom you may disagree, or find it difficult to cooperate. Accept that, but even so, remain respectful. Disagreement is no excuse for poor behaviour or personal attacks, and a community in which people feel threatened is not a healthy community.

1. **Assume good faith**

   Free Software Contributors have many ways of reaching our common goal of producing Free Software for the world which may differ from your ways. Assume that other people are working towards this goal.

   Note that many of our Contributors are not native English speakers or may have different cultural backgrounds.

1. **Be collaborative**

   Free Software Community is large and complex; there is always more to learn. It's good to ask for help when you need it. Similarly, offers for help should be seen in the context of our shared goal of improving Free Software projects.

   When you make something for the benefit of the project, be willing to explain to others how it works, so that they can build on your work to make it even better.

1. **Try to be concise and on-topic**

   Keep in mind that what you write once will be read by hundreds of persons. Writing a short email means people can understand the conversation as efficiently as possible. When a long explanation is necessary, consider adding a summary.

   Try to bring new arguments to a conversation so that each mail adds something unique to the thread, keeping in mind that the rest of the thread still contains the other messages with arguments that have already been made.

   Try to stay on topic, especially in discussions that are already fairly large.

1. **Be open**

   Most ways of communication used within Free Software community allow for public and private communication.

   Using public forums for Free Software related messages is much more likely to result in responses. It also makes sure that any inadvertent mistakes made by people responding will be more easily detected and corrected. This also creates a learning opportunity for everyone.

   If you use private messages to discuss Free Software related issues due to personal reasons or because you're discussing sensitive information, respect the other person's personal space and privacy. Do not demand a response, especially if the other person lets you know that they are not in a position to respond.

   If you do not want to receive private messages from others, try to include that information in your profile if the communication platform supports it. If you receive a private message that you do not want to respond to, consider sending the link fsci.in/dnd in response.

1. **Be patient**

    Free Software projects are maintained by contributors in the community who have different priorities in their own life. If you make a request/suggestion, or ask a question, there is no obligation for anyone to respond or to accept your request/suggestion. If you feel that you're being not heard, try to find a way forward on your own and/or wait patiently.

1. **In case of problems**

    While this code of conduct should be adhered to by participants, we recognize that sometimes people may have a bad day, or be unaware of some of the guidelines in this code of conduct. When that happens, you may reply to them and point out this code of conduct. Such messages may be in public or in private, whatever is most appropriate. However, regardless of whether the message is public or not, it should still adhere to the relevant parts of this code of conduct; in particular, it should not be abusive or disrespectful. Assume good faith; it is more likely that participants are unaware of their bad behaviour than that they intentionally try to degrade the quality of the discussion.

    Serious or persistent offenders will be temporarily or permanently banned from communicating through FSCI's systems. Complaints should be made (in private) to the [administrators](mailto:admin@fsci.in) of the FSCI communication forum in question. To find contact information for these administrators, please see the page on FSCI's organizational structure.


This document is a result of collaborative work by the FSCI volunteers based on the following. If you feel like something needs to be added or changed feel free to propose it [here](https://codema.in/d/o5fnJXGU/create-an-inclusion-policy-for-fsci):

* https://web.archive.org/web/20190721195622/https://community-covenant.net/version/1/0/
* https://www.un.org/en/gender-inclusive-language/guidelines.shtml
* https://www.mozilla.org/en-US/about/governance/policies/participation/
* https://www.debian.org/code_of_conduct

This document is licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
